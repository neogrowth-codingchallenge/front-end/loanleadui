export class CommunicationLog {
  id: number;
  communicationDate?: Date;
  communicationMode: string;
  leadStatus: string;
  leadId: number;
}
