export class OrganizationInfo {
  id: number;
  name: string;
  website: string;
  natureOfBusiness: string;
}
