import { ContactInfo } from './contactInfo.model';
import { LoanLead } from './loanlead.model';
import { OrganizationInfo } from './organizationInfo.model';

export class LeadDetail {
  organizationInfo: OrganizationInfo;
  contactInfo: ContactInfo;
  LeadInfo: LoanLead;
}
