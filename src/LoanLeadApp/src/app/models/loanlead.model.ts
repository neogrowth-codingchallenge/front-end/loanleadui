export class LoanLead {
  id: number;
  contactId: number;
  loanAmount: number;
  communicationMode: string;
  status: string;
  source: string;
}
