export class ContactInfo {
  id: number;
  OrganizationId: number;
  name: string;
  contactType: string;
  gender: string;
  emailId: string;
  phoneNumber: string;
  dob: Date;
}
