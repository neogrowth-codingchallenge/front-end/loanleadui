/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LoanleadService } from './loanlead.service';

describe('Service: Loanlead', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoanleadService]
    });
  });

  it('should ...', inject([LoanleadService], (service: LoanleadService) => {
    expect(service).toBeTruthy();
  }));
});
