import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoanleadService {
  baseUrl = 'http://localhost:5503/api/loanleads/';

  constructor(private http: HttpClient) {}

  getLeads() {
    return this.http.get(this.baseUrl);
  }
}
