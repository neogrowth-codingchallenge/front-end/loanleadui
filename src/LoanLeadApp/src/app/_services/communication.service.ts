import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { CommunicationLog } from '../models/communicationlog.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CommunicationService {
  baseUrl = 'http://localhost:5503/api/Communications/';
  constructor(private http: HttpClient) {}

  getLogs(leadId: number) {
    return this.http.get(this.baseUrl + leadId);
  }

  AddLog(model: CommunicationLog) {
    return this.http.post(this.baseUrl, model);
  }
}
