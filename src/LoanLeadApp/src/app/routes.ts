import { Router, Routes } from '@angular/router';
import { AddLeadComponent } from './add-lead/add-lead.component';
import { ListCommunicationsComponent } from './communications/list-communications.component';
import { HomeComponent } from './home/home.component';
import { LeadComponent } from './Lead/Lead.component';
import { CommunicationService } from './_services/communication.service';

export const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'leads', component: LeadComponent },
  { path: 'add-lead', component: AddLeadComponent },
  { path: 'logs', component: ListCommunicationsComponent },
  { path: '**', component: HomeComponent },
];
