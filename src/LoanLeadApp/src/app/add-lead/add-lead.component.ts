import { Component, OnInit } from '@angular/core';
import { LeadDetail } from '../models/leadDetail.model';
import { LoanLead } from '../models/loanlead.model';
import { LoanleadService } from '../_services/loanlead.service';

@Component({
  selector: 'app-add-lead',
  templateUrl: './add-lead.component.html',
  styleUrls: ['./add-lead.component.css'],
})
export class AddLeadComponent implements OnInit {
  leads: LoanLead[];
  leadSources: string[] = [
    'Emailer',
    'Direct Sales Agent',
    'NewsPaper',
    'Marketing',
  ];

  genders: string[] = ['Male', 'Female', 'Others'];
  communicationModes: string[] = ['Call', 'Personal Meet'];
  leadDetail: LeadDetail;

  constructor(private service: LoanleadService) {}

  ngOnInit() {
    this.service.getLeads().subscribe((data) => {
      this.leads = data as LoanLead[];
    });
  }

  addLead() {
    console.log('Adding lead');
  }
}
