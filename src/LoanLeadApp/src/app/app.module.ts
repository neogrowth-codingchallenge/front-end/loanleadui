import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LeadComponent } from './Lead/Lead.component';
import { NavComponent } from './nav/nav.component';
import { ListCommunicationsComponent } from './communications/list-communications.component';
import { CommunicationService } from './_services/communication.service';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { AddLeadComponent } from './add-lead/add-lead.component';

@NgModule({
  declarations: [
    AppComponent,
    LeadComponent,
    NavComponent,
    ListCommunicationsComponent,
    HomeComponent,
    AddLeadComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [CommunicationService],
  bootstrap: [AppComponent],
})
export class AppModule {}
