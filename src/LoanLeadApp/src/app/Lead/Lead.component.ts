import { Component, OnInit } from '@angular/core';
import { LoanLead } from '../models/loanlead.model';
import { LoanleadService } from '../_services/loanlead.service';

@Component({
  selector: 'app-Lead',
  templateUrl: './Lead.component.html',
  styleUrls: ['./Lead.component.scss'],
})
export class LeadComponent implements OnInit {
  leads: LoanLead[];
  selectedId: number;
  showLogs: boolean;
  constructor(private service: LoanleadService) {}

  ngOnInit() {
    this.service.getLeads().subscribe((data) => {
      this.leads = data as LoanLead[];
    });
  }

  selectLead(event : any) {
    this.selectedId = event;
    console.log(this.selectedId);
    this.showLogs = true;
  }
}
