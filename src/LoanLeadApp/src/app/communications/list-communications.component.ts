import { Component, Input, OnInit } from '@angular/core';
import { CommunicationLog } from '../models/communicationlog.model';
import { CommunicationService } from '../_services/communication.service';

@Component({
  selector: 'app-list-communications',
  templateUrl: './list-communications.component.html',
  styleUrls: ['./list-communications.component.css'],
})
export class ListCommunicationsComponent implements OnInit {
  @Input() selectedLeadId: number;
  communications: CommunicationLog[];
  addEnable = false;
  communicationModes: string[] = ['Call', 'Personal Meet'];
  constructor(private service: CommunicationService) {}

  ngOnInit(): void {
    this.loadData();
  }

  ngOnChanges(): void {
    this.loadData();
  }

  loadData() {
    this.addEnable = false;
    this.service.getLogs(this.selectedLeadId).subscribe((data) => {
      this.communications = data as CommunicationLog[];
    });
  }

  toggleAdd() {
    
    this.addEnable = !this.addEnable;
  }
}
